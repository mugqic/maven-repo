# Mcgill University and Genome Quebec (MUGQIC) bioinformatics maven repository

Welcome to our public artifacts repository. You can use it with maven, sbt, ivy or another compatible dependency manager.

## Use it with **maven**

In your ```pom.xml```, add this snippet:
  
```
<repositories>
    <repository>
        <id>mugqic-bitbucket</id>
        <name>MUGQIC maven repository</name>
        <url>https://bitbucket.org/mugqic/maven-repo/raw/master/</url>
        <layout>default</layout>
    </repository>
</repositories>
```
  
## Use it with **sbt**

In build.sbt, add the following:

```
resolvers += "mugqic-bitbucket" at "https://bitbucket.org/mugqic/maven-repo/raw/master/"
```  
  
## Use it with **ivy**

Use a resolver like the following:

```
<ibiblio
    name="mugqic-bitbucket"
    root="https://bitbucket.org/mugqic/maven-repo/raw/master/"
    m2compatible="true" />
```

A full example is available on this maven repository.

## Add dependencies by doing:
```
mvn clean install source:jar javadoc:jar
mvn deploy:deploy-file -Durl=file://<CLONED_PATH> -DrepositoryId=mugqic-repo -Dfile=target/picard-1.96.jar -DgroupId=net.sf.picard -DartifactId=picard -Dversion=1.96 -Dpackaging=jar -DgeneratePom=true -Dsources=target/picard-1.96-src.jar -Djavadoc=target/picard-doc.jar
```
change ```generatePom=true``` with ```pomFile=pom.xml``` if you have one.

