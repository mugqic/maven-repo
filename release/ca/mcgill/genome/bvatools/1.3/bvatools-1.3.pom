<?xml version="1.0" encoding="ISO-8859-1"?>
<!--

    This file is part of BVATools.

    BVATools is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BVATools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with BVATools.  If not, see <http://www.gnu.org/licenses/>.

-->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<!--============================================================================================= -->
	<!-- Project Information -->
	<!--============================================================================================= -->
	<name>BVATools</name>
	<description>BAM and Variant Analyzer Tools</description>
	<url />
	<inceptionYear>2010</inceptionYear>
	<licenses>
		<license>
			<name>GNU Library or Lesser General Public License (LGPL)</name>
			<url>http://http://www.gnu.org/licenses/lgpl-3.0.html</url>
		</license>
	</licenses>
	<developers>
		<developer>
			<id>lletourn</id>
			<name>Louis Letourneau</name>
			<email>louis.letourneau@mail.mcgill.ca</email>
			<organization>Genome Quebec</organization>
			<organizationUrl>http://genomequebec.com</organizationUrl>
			<roles>
				<role>software engineer</role>
				<role>bioinformatician</role>
			</roles>
			<timezone>-5</timezone>
		</developer>
		<developer>
			<id>mcaron</id>
			<name>Louis Letourneau</name>
			<email>max.caron@mail.mcgill.ca</email>
			<organization>McGill University</organization>
			<organizationUrl>http://www.mcgill.ca/humangenetics/</organizationUrl>
			<roles>
				<role>bioinformatician</role>
			</roles>
		</developer>
		<developer>
			<id>eaudemard</id>
			<name>Eric Audemard</name>
			<email>eric.audemard@mail.mcgill.ca</email>
			<organization>McGill University</organization>
			<organizationUrl>http://www.mcgill.ca/humangenetics/</organizationUrl>
			<roles>
				<role>bioinformatician</role>
			</roles>
		</developer>
	</developers>
	<contributors />
	<organization>
		<name>Genome Quebec</name>
		<url>http://www.genomequebec.com</url>
	</organization>

	<!--============================================================================================= -->
	<!-- POM Relationships -->
	<!--============================================================================================= -->
	<groupId>ca.mcgill.genome</groupId>
	<artifactId>bvatools</artifactId>
	<version>1.3</version>
	<packaging>jar</packaging>

	<dependencyManagement />

	<dependencies>
		<dependency>
			<groupId>net.sf.picard</groupId>
			<artifactId>picard</artifactId>
			<version>1.107</version>
		</dependency>
		<dependency>
			<groupId>net.sf.samtools</groupId>
			<artifactId>sam</artifactId>
			<version>1.107</version>
		</dependency>
		<dependency>
			<groupId>org.broad.tribble</groupId>
			<artifactId>tribble</artifactId>
			<version>1.107</version>
		</dependency>
		<dependency>
			<groupId>org.broadinstitute.variant</groupId>
			<artifactId>variant</artifactId>
			<version>1.107</version>
		</dependency>

		<dependency>
			<groupId>ca.mcgill.mcb.pcingola</groupId>
			<artifactId>snpEff</artifactId>
			<version>3.4</version>
			<exclusions>
				<exclusion>
					<groupId>net.sf.samtools</groupId>
					<artifactId>Sam</artifactId>
				</exclusion>
				<exclusion>
					<groupId>net.sf.picard</groupId>
					<artifactId>Picard</artifactId>
				</exclusion>
				<exclusion>
					<groupId>log4j</groupId>
					<artifactId>log4j</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>org.obiba.genobyte</groupId>
			<artifactId>genobyte</artifactId>
			<version>1.4.3</version>
			<exclusions>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>slf4j-api</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>jcl104-over-slf4j</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>net.sf.trove4j</groupId>
			<artifactId>trove4j</artifactId>
			<version>3.0.3</version>
		</dependency>

		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-math3</artifactId>
			<version>3.2</version>
		</dependency>
		<dependency>
			<groupId>commons-cli</groupId>
			<artifactId>commons-cli</artifactId>
			<version>1.1</version>
		</dependency>
		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>1.4</version>
		</dependency>
		<dependency>
			<groupId>commons-lang</groupId>
			<artifactId>commons-lang</artifactId>
			<version>2.6</version>
		</dependency>

		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>jcl-over-slf4j</artifactId>
			<version>1.5.3</version>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-simple</artifactId>
			<version>1.5.3</version>
		</dependency>

		<dependency>
			<groupId>net.sf.opencsv</groupId>
			<artifactId>opencsv</artifactId>
			<version>2.3</version>
		</dependency>

		<dependency>
			<groupId>org.jfree</groupId>
			<artifactId>jfreechart</artifactId>
			<version>1.0.14</version>
		</dependency>
                <dependency>
                        <groupId>org.jfree</groupId>
                        <artifactId>jfreesvg</artifactId>
                        <version>1.8</version>
                </dependency>

	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>2.3.2</version>
				<configuration>
					<source>1.7</source>
					<target>1.7</target>
					<encoding>ISO-8859-1</encoding>
					<showDeprecation>true</showDeprecation>
					<showWarnings>true</showWarnings>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-assembly-plugin</artifactId>
				<version>2.3</version>
				<configuration>
					<descriptorRefs>
						<descriptorRef>jar-with-dependencies</descriptorRef>
					</descriptorRefs>
					<archive>
						<manifest>
							<mainClass>ca.mcgill.genome.bvatools.BVATools</mainClass>
							<addDefaultImplementationEntries>true</addDefaultImplementationEntries>
						</manifest>
					</archive>
				</configuration>
			</plugin>
			<plugin>
				<groupId>com.mycila</groupId>
				<artifactId>license-maven-plugin</artifactId>
				<version>2.5</version>
				<configuration>
					<header>com/mycila/maven/plugin/license/templates/LGPL-3.txt</header>
					<properties>
						<owner>Louis Letourneau</owner>
						<year>${project.inceptionYear}</year>
						<email>louis.letourneau@mail.mcgill.ca</email>
					</properties>
					<excludes>
						<exclude>**/README</exclude>
						<exclude>src/test/resources/**</exclude>
						<exclude>src/main/resources/**</exclude>
						<exclude>src/main/scripts/**</exclude>
						<exclude>src/main/java/ca/mcgill/genome/bvatools/util/NaturalOrderComparator.java</exclude>
						<exclude>LICENSE.txt</exclude>
						<exclude>README*</exclude>
					</excludes>
				</configuration>
				<executions>
					<execution>
						<goals>
							<goal>check</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>

	<repositories>
		<repository>
			<id>mugqic-bitbucket</id>
			<url>http://bitbucket.org/mugqic/maven-repo/raw/master/release</url>
		</repository>
		<repository>
			<id>obiba.org</id>
			<name>Obiba Maven 2.x Repository</name>
			<url>http://maven.obiba.org/maven2</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
	</repositories>

	<scm>
		<connection>scm:git:ssh://git@bitbucket.org:mugqic/bvatools.git</connection>
		<developerConnection>scm:git:ssh://git@bitbucket.org:mugqic/bvatools.git</developerConnection>
		<url>https://bitbucket.org/mugqic/bvatools</url>
	</scm>

	<properties>
		<project.build.sourceEncoding>ISO-8859-1</project.build.sourceEncoding>
		<skipTests>true</skipTests>
	</properties>
</project>
